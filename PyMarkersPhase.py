#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 30 15:29:27 2021

@author: lucas
"""

import os,sys
import laTools 
import numpy as np
import gridInterpolator

import vtkTools as v

"""
This is an example script on how to save your markerfiles using Python3
    - The geometry is stored in the lamemGrid() class
    - X,Y,Z, phase and temperature are then stored to the markerfiles
    - Phase and temperature a filled with placeholder values and
      can be modified by replacing lamemGrid.Phase and
      lamemGrid.Temp with similar sized arrays
    - One example for replacing Phase is provided using SciPy
      regular grid interpolator and one using meshes from .stl files 
    - There is also an option to write a VTK file to vizualize the Phase
"""

inFile = "input/input.dat"

#directory of partitioning file (created with mode save_grid)
partFile ="input/ProcessorPartitioning_4cpu_1.2.2.bin"

#creating the class storing all the information
lamemGrid = laTools.createGrid(inFile, partFile)



#========examples for replacing the Phase=========

#====Gridinterpolator=========

#extracting the LaMEM grid from the class
#totalCoors = len(lamemGrid.Xpart.flatten('C'))
# gridVals = np.array([lamemGrid.Xpart, lamemGrid.Ypart, lamemGrid.Zpart])

# loading exampe phase and grid from array
# geGrid = np.load("example/testGrid.npy")
# gePhase = np.load("example/testPhase.npy")

# X = geGrid[0]
# Y = geGrid[1]
# Z = geGrid[2]

# #interpolating onto the LaMEM grid
# Phase = gridInterpolator(X ,Y, Z, gePhase, gridVals)

#====3d meshes=============

#stl file containig the polygon
mesh = list(["input/fox.stl","input/cat.stl"])
names = list(["fox","cat"])


#Label = {"Phase":"Fox","Number": 1 ,
          #"Phase" :"Cat","Number" : 2}
#getting the new phase array and all the points(only useful for plotting)
#Phase, Points, info = laTools.stl2Phase(lamemGrid,mesh,names,"m", True)

#replacing the palceholder phase IDs
#lamemGrid.Phase = Phase
#with this option vtk file can be written
#v.writeVTK(lamemGrid)
#=======================================

#save to markerfiles                         
laTools.SaveMarkers(partFile,lamemGrid, a ="APS" )
