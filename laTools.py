#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 19 11:28:15 2021

@author: lucas
"""
import sys, os
import numpy as np


def FDSTAGMeshGeneratorPython(nmark_x, nmark_y, nmark_z, filename, IS64BIT = False, Randomnoise = False ):
    
    nProcX,nProcY,nProcZ,nNodeX,nNodeY,nNodeZ,ix,iy,iz,xcoor,ycoor,zcoor,xc,yc,zc = GetProcessorPartitioning(filename)
   
    
    dx = np.subtract(xcoor[1:], xcoor[0:-1])
    dy = np.subtract(ycoor[1:], ycoor[0:-1])
    dz = np.subtract(zcoor[1:], zcoor[0:-1])

    #Create empty arrays for x,y,z for the meshgrid later
    x = np.empty(0)
    y = np.empty(0)
    z = np.empty(0)


    #compute the x,y,z values
    for i in range (dx.size):
        for j in range(nmark_x):            
            if  j == 0 and i == 0 :
                a = xcoor[0]+dx[i]*0.1
                #a = dx[i]/nmark_x*0.5
            elif j==0 and i != 0 :
                a = x[-1] +dx[i-1]/nmark_x*0.5 + dx[i]/nmark_x*0.5
            else:    
                a = x[-1] + dx[i]/nmark_x
            x = np.append([x], a)

    for i in range (dy.size):
        for j in range(nmark_y):
            if  j == 0 and i == 0 :
                a = ycoor[0]+dx[i]*0.1
                #a = dy[i]/nmark_y*0.5
            elif j==0 and i != 0:
                a = y[-1] +dy[i-1]/nmark_y*0.5 + dy[i]/nmark_y*0.5
            else:
                a = y[-1] + dy[i]/nmark_y
            y = np.append([y], a)

    for i in range (dz.size):
        for j in range(nmark_z):
            if  j == 0 and i == 0 :
                a = zcoor[0]+dx[i]*0.1
                #a = dz[i]/nmark_z*0.5
            elif j==0 and i != 0:
                a = z[-1] +dz[i-1]/nmark_z*0.5 + dz[i]/nmark_z*0.5
            else:
                a = z[-1] + dz[i]/nmark_z
            z = np.append([z], a)


    #create Meshgrid
    X, Y, Z = np.meshgrid(x,y,z)    
    #RandomNoise to do!!!
   
    Xpart = X
    Ypart = Y
    Zpart = Z
                                                                        
    return  X, Y, Z, xcoor, ycoor, zcoor, Xpart, Ypart, Zpart  



import struct



# func to get the scheme of the processors
def get_numscheme(nProcX, nProcY, nProcZ):
    num = 0
    n = []
    nix = []
    njy = []
    nkz = []
    for k in range (0, nProcZ):
        for j in range (0, nProcY):
            for i in range (0, nProcX):
                num = num +1
                n = np.append([n],num)
                nix = np.append([nix], i)
                njy = np.append([njy], j)
                nkz = np.append([nkz], k)
                
    return n, nix, njy, nkz

#func to get the indicies of the processor grid
def  get_ind(x, xc, nProc):
    xi = []
    ix_start = np.array([0])
    ix_end = []
    if nProc == 1:
        xi = np.array(x.shape)
        ix_start = np.array([0])
        ix_end = np.array([x.size])
    else:
        for k in range(0, nProc):
            if k == 0:
                count = 0
                for i in range(0,x.size):
                    
                    if x[i] >= xc[k] and x[i] <= xc[k+1]:
                        count = count+1
                xi = np.append([xi], count)
               
            else:
                count = 0
                for i in range (0,x.size):
                    
                    if x[i] > xc[k] and x[i] <= xc[k+1]:
                        count = count +1
                xi = np.append([xi], count)
        start = np.cumsum(xi)
        ix_start = np.hstack((ix_start,start))
        ix_start = ix_start[:-1]
        ix_end = np.cumsum(xi)
    return xi, ix_start, ix_end

def SaveMarkers(filename, Input_Geometry,**kwargs):
    #number of properites markers carry(x,y,z,phase, temp)
    
    adFeat = list(kwargs.values())
    num_prop = 5

    
    nProcX,nProcY,nProcZ,nNodeX,nNodeY,nNodeZ,ix,iy,iz,xcoor,ycoor,zcoor,xc,yc,zc = GetProcessorPartitioning(filename)
    
    #remove unused variables from ProcessorPartitioning(might casue troubles later)
    del nNodeX,nNodeY, nNodeZ, ix,iy,iz,xcoor,ycoor, zcoor
    #Input_Geometry = create_geom
    x = Input_Geometry.x
    y = Input_Geometry.y
    z = Input_Geometry.z
    
    X = Input_Geometry.Xpart
    Y = Input_Geometry.Ypart
    Z = Input_Geometry.Zpart
    Phase = Input_Geometry.Phase
    Temp = Input_Geometry.Temp
    APS = Input_Geometry.APS


    #get total number of rocessors
    Nproc = nProcX*nProcY*nProcZ
    
    #calculate the gridparameters
    num, num_i, num_j, num_k = get_numscheme(nProcX, nProcY, nProcZ)
    
    xi,ix_start,ix_end = get_ind(x,xc,nProcX)
    yi,iy_start,iy_end = get_ind(y,yc,nProcY)
    zi,iz_start,iz_end = get_ind(z,zc,nProcZ)
    
    
    #calculate the start end end coors of x,y,z
    z_start = np.array([])
    z_end = np.array([])
    y_start = np.array([])
    y_end = np.array([])
    x_start = np.array([])
    x_end = np.array([])
    for i in range(0, num.size):
        x_start = np.append([x_start], ix_start[num_i[i].astype(int)])
        x_end = np.append([x_end],ix_end[num_i[i].astype(int)])
            
        y_start = np.append([y_start],iy_start[num_j[i].astype(int)])
        y_end = np.append([y_end],iy_end[num_j[i].astype(int)])
    
        z_start = np.append([z_start],iz_start[num_k[i].astype(int)])
        z_end = np.append([z_end],iz_end[num_k[i].astype(int)])
    
    
        
    #write x,y,z.phase,temp(interlace format)
        #add one double value as header
        #phase as float64/double 
    
    #write the markers to a binary file
        #structure :
        #-silent header(float64)
        #-number of particles 
        #-[x,y,z,phase,temp] in interlaced format(float64)
        #binary format is big endian ">d"
        
    #path = os.getcwd()
    name = filename.split("_",1)
    name = name[1]
    name = name.split(".bin")
    name = name[0]
    name = name.replace(".","_")
    dir_name = 'Markers_' + str(name)
    print("Saving markerfiles....")
    print("--------------------------")
    print("Markerfile directory: output/" + dir_name)
    if not os.path.exists('output'):
        os.mkdir('output')
    os.chdir("output")
    
    if not os.path.exists(dir_name):
        os.mkdir(dir_name)
    
        
    for i in range(0,Nproc):
        
    
        part_x = X[x_start[i].astype(int) : x_end[i].astype(int), y_start[i].astype(int) : y_end[i].astype(int),
                    z_start[i].astype(int) : z_end[i].astype(int)]
        part_y = Y[x_start[i].astype(int) : x_end[i].astype(int), y_start[i].astype(int) : y_end[i].astype(int),
                    z_start[i].astype(int) : z_end[i].astype(int)]
        part_z = Z[x_start[i].astype(int) : x_end[i].astype(int), y_start[i].astype(int) : y_end[i].astype(int),
                    z_start[i].astype(int) : z_end[i].astype(int)]
        part_phs = Phase[x_start[i].astype(int) : x_end[i].astype(int), y_start[i].astype(int) : y_end[i].astype(int),
                    z_start[i].astype(int) : z_end[i].astype(int)]
        part_temp = Temp[x_start[i].astype(int) : x_end[i].astype(int), y_start[i].astype(int) : y_end[i].astype(int),
                    z_start[i].astype(int) : z_end[i].astype(int)]
        if "APS" in adFeat:
            part_APS = APS[x_start[i].astype(int) : x_end[i].astype(int), y_start[i].astype(int) : y_end[i].astype(int),
                        z_start[i].astype(int) : z_end[i].astype(int)]
    
        num_part_x = part_x.shape[0] 
        num_part_y = part_x.shape[1]
        num_part_z = part_x.shape[2]
        num_part = num_part_x*num_part_y*num_part_z
    
        header = 69
        lvec_info=np.array([num_part])
        output = np.array(())
        output =np.append([output], header)
        output = np.append([output], lvec_info)
        
        part_x = part_x.flatten('F')
        part_y = part_y.flatten('F')
        part_z = part_z.flatten('F')
        part_phs = part_phs.flatten('F')
        part_temp = part_temp.flatten('F')
        if "APS" in adFeat:

            part_APS = part_APS.flatten('F')
        
        interlace = np.array([])
    
    
        for j in range(0,num_part):
            interlace = np.append([interlace], part_x[j])
            interlace = np.append([interlace], part_y[j])
            interlace = np.append([interlace], part_z[j])
            interlace = np.append([interlace], part_phs[j])
            interlace = np.append([interlace], part_temp[j])
            if "APS" in adFeat:

                interlace = np.append([interlace], part_APS[j])
            output = np. append([output], interlace)
            interlace = np.array([])
    
        count = str(i)
        #1 1 1     
        fname = "Markers." + count.zfill(8)
        fname = fname +".dat"
        Markers = open(os.path.join(dir_name, fname), "wb")
       
        for l in range(0,output.size):
            A = struct.pack(">d",output[l])
            Markers.write(A)
        Markers.close()
    return






from scipy.interpolate import RegularGridInterpolator as rgi


import math as m



def distance_plane(origin, normal, points):
    """
    This function computes the closest linear distance from any 3D point to a plane
    origin: starting point of the plane normal
    normal: plane normal 
    points: 3xn array with n points to compute the distance for
    returns : 3xn array with n distance values
    """

    distance = np.array([])
    A = normal[0]
    B = normal[1]
    C = normal[2]
    x = origin[0]
    y = origin[1]
    z = origin[2]
 
    D = -(normal[0]*origin[0])-(normal[1]*origin[1])-(normal[2]*origin[2])
    D = -(A*x)-(B*y)-(C*z)
    for i in range(points.shape[0]):
        dis_1 = abs((normal[0]*points[i][0])+(normal[1]*points[i][1])+(normal[2]*points[i][2])+D)
        dis_2 = m.sqrt(normal[0]**2 + normal[1]**2 + normal[2]**2)
        dis = dis_1/dis_2

        distance = np.append([distance], dis)     
    return distance 



def createGrid(paramfile, filename, **kwargs):#,X, Y, Z, Phase, Grid):
    
    adFeat = list(kwargs.values())
    
    print(adFeat)
    class lamemGrid(object):

        def __init__(self, width_q, length_q, height_q, nump_x,nump_y,nump_z,
                     x,y,z,Xpart,Ypart,Zpart,nmark_x,nmark_y,nmark_z,Phase, Temp,APS):
            self.w = width_q
            self.l = length_q
            self.h = height_q
    
            self.nump_x = nump_x
            self.nump_y = nump_y
            self.nump_z = nump_z
    
            self.x = x
            self.y = y
            self.z = z 
    
            self.Xpart = X
            self.Ypart = Y
            self.Zpart = Z
    
            self.npart_x = nmark_x
            self.npart_y = nmark_y
            self.npart_z = nmark_z
    
            self.Phase = Phase 
            self.Temp = Temp
            self.APS = APS
            
            

    
            return 
        
    # if "APS" in adFeat:
    #     lamemGrid.APS = property(lambda self: self.Temp )
    
    #reading initial Paramters
    units, LaMEM_start, LaMEM_end, nmark_x, nmark_y, nmark_z, nel_x, nel_y, nel_z = readParams(paramfile)


           
            
    X, Y, Z, xcoor, ycoor, zcoor, Xpart, Ypart, Zpart  = FDSTAGMeshGeneratorPython(nmark_x, nmark_y, nmark_z,
                                                                                 filename,False,False)
    
    
    print("Creating meshgrid....")
    print("-----------------------")
    print("Grid parameters:")
    print("-----------------------")
    print("Number of markers per cell:")
    print("nmark_x = " , nmark_x)
    print("nmark_y = " , nmark_y)
    print("nmark_z = " , nmark_z)
    print("-----------------------")
    print("Number of cells:")
    print("X-direction :", nel_x)
    print("Y-direction :", nel_y)
    print("Z-direction :", nel_z)
    print("-----------------------")
    print("X-coordinate boundaries:")
    print(LaMEM_start[0], LaMEM_end[0])
    print("Y-coordinate boundaries:")
    print(LaMEM_start[1], LaMEM_end[1])
    print("Z-coordinate boundaries:")
    print(LaMEM_start[2], LaMEM_end[2])
    print("-----------------------")
    
    scalefactor = 1000
    if 'geo' in units:
        LaMEM_start = LaMEM_start * scalefactor
        LaMEM_end = LaMEM_end * scalefactor
        
            #compute and print the modelresolution
    resolution_x = (LaMEM_end[0]-LaMEM_start[0])/nel_x
    resolution_y = (LaMEM_end[1]-LaMEM_start[1])/nel_y
    resolution_z = (LaMEM_end[2]-LaMEM_start[2])/nel_z

    
    print("Resolution(in meters) of the LaMEM model in X, Y, Z is:", 
          resolution_x,",", resolution_y,",", resolution_z)
     
    print("-----------------------")
    
    
    #update other variables
    nump_x = X.shape[1]
    nump_y = X.shape[0]
    nump_z = X.shape[2]
    
    #===========Domain parameters==========
    
    length_q = xcoor[-1] - xcoor[0]
    width_q = ycoor[-1] - ycoor[0]
    height_q = zcoor[-1] -zcoor[0]
    
    x = X[1,:,1]
    y = Y[:,1,1]
    z = Z[1,1,:]
    
    
    X = np.transpose(X, (1,0,2)) 
    Y = np.transpose(Y, (1,0,2))
    Z = np.transpose(Z, (1,0,2))
    
    Phase = np.ones_like(X)

    APS = np.zeros_like(X)
    
    #create 3d array of the point where we want to interpolate
    #pts = np.array([X.flatten(),Y.flatten(),Z.flatten()])
    
    #transpose 
    #pts = np.transpose(pts)
    
    #X_L, Y_L, Z_L, Phase_g = gempyModel(extent_g, res_g, 1,  False, True)
    #if 'geo' in units:
    #    X = X * 1e-3
    #    Y = Y * 1e-3
    #    Z = Z * 1e-3
   
    #define interpolation function  & interpolate
    #ipdb.set_trace()
    #interp3 = rgi((X, Y, Z), Phase, method="linear")
    #Phase = interp3(pts)
    #APS = np.zeros_like(Phase)

    #extract wether APS occurs or not
    # interp3_APS = rgi((X_L, Y_L, Z_L), APS, method = "linear")
    # APS = interp3_APS(pts)
    
    
   
    
    #normal = np.reshape(normal, (n_faults,3))
#    origin = np.reshape(origin, (n_faults,3))
#    #ipdb.set_trace()
#    res_x = resolution_x*1e-3
#    res_y = resolution_y*1e-3
#
#    res_z = resolution_z*1e-3
#    APS_D = APS_D*1e-3
#
#    for k in range(n_faults):
#        
#        #normal_ = normal[k,]*1e-3
#        origin_ = origin[k,]*1e-3
#        #ipdb.set_trace()
#        #due to reasons i don't understand azimuth and dip are mixed up
#        # if dip_F[k] == 90:
#        #     normal_ = (origin_[0]+res_x, origin_[1], origin_[2]+(res_z*m.cos(m.radians(90 -azi_F[k])))) 
#        # else:
#        #     normal_ = (origin_[0]-res_x, origin_[1], origin_[2]+(res_z*m.cos(m.radians(90- azi_F[k]))))
#        
#        normal_1 = (( ((-m.sin(m.radians(dip_F[k])))*(m.sin(m.radians(azi_F[k]))))))
#        normal_2 = (( (m.sin(m.radians(dip_F[k])))*(m.cos(m.radians(azi_F[k])))))
#        normal_3 = (( (-m.cos(m.radians(dip_F[k])))))
#        normal_ = np.array([normal_1, normal_2, normal_3])
#        #ipdb.set_trace()
#        distance = distance_plane(origin= origin_, normal= normal_, points =pts)
#        for p in range(distance.size):
#            if distance[p]<=APS_D:
#                Phase[p]= k+1
#            # elif distance[p]<= APS_D:
#                APS[p] = 0.5
    
    #again round of to integers as rgi gives decimals
    Phase = np.rint(Phase)

    #  #again reshape the phase to the intial LaMEM dimensions
    Phase = np.reshape(Phase,(nump_x, nump_y, nump_z))

    APS = np.reshape(APS,(nump_x, nump_y, nump_z))
                  
    #temp of the mantle
    T_mantle = 1350
    Temp = np.multiply(T_mantle,np.ones_like(X))
        

    lamem_Grid = lamemGrid(width_q, length_q, height_q, nump_x, nump_y, 
                                   nump_z,x,y,z,Xpart,Ypart,Zpart,nmark_x,nmark_y,nmark_z,Phase,Temp,APS)
    
        #ipdb.set_trace()
    return lamem_Grid







import struct


#passed by main script
#filename = 'ProcessorPartitioning_1cpu_1.1.1.bin'

def GetProcessorPartitioning(filename, IS64Bit= False):
    #open the file
    f = open(filename, "rb")
    #check for python version
    
    if sys.version_info[0] < 3:
    
    #set the float precision, > is big endian
        if IS64Bit:
            precision_Int      = ">i8"
            precision_Scalar   = ">f8"
        else:
            precision_Int      = ">i4"
            precision_Scalar   = ">d"
    else: 
        if IS64Bit:
            precision_Int      = ">i"
            precision_Scalar   = ">f"
        else:
            precision_Int      = ">i"
            precision_Scalar   = ">d"
        
        
    #first extract number of processors in x,y,z
    #f.read() reads the binary, struct.unpack converts it into an integer
    #ipdb.set_trace()
    nProcX = struct.unpack(precision_Int, f.read(struct.calcsize(precision_Int)))[0]

    nProcY = struct.unpack(precision_Int, f.read(struct.calcsize(precision_Int)))[0]

    nProcZ = struct.unpack(precision_Int, f.read(struct.calcsize(precision_Int)))[0]

    #number of nodes 
    nNodeX = struct.unpack(precision_Int, f.read(struct.calcsize(precision_Int)))[0]

    nNodeY = struct.unpack(precision_Int, f.read(struct.calcsize(precision_Int)))[0]

    nNodeZ = struct.unpack(precision_Int, f.read(struct.calcsize(precision_Int)))[0]
    
    
    #first node indices
    #introducing a format string as the size depends on the number of processors
    formatString = precision_Int[0]+str(nProcX+1)+precision_Int[1:]
    readBuf = f.read(struct.calcsize(formatString))
    ix = np.array(struct.unpack(formatString,readBuf))
   
    formatString = precision_Int[0]+str(nProcY+1)+precision_Int[1:]
    readBuf = f.read(struct.calcsize(formatString))
    iy = np.array(struct.unpack(formatString,readBuf))

    formatString = precision_Int[0]+str(nProcZ+1)+precision_Int[1:]
    readBuf = f.read(struct.calcsize(formatString))
    iz = np.array(struct.unpack(formatString,readBuf))

    #coordinate scaling
    charLength = struct.unpack(precision_Scalar, f.read(struct.calcsize(precision_Scalar)))[0]

    #coordinates of the nodes
    #dpends on the number of nodes
    #already multiplied with scaling factor
    formatString = precision_Scalar[0]+str(nNodeX)+precision_Scalar[1:]
    readBuf = f.read(struct.calcsize(formatString))
    xcoor = struct.unpack(formatString,readBuf)
    xcoor = np.multiply(xcoor,charLength)

    formatString = precision_Scalar[0]+str(nNodeY)+precision_Scalar[1:]
    readBuf = f.read(struct.calcsize(formatString))
    ycoor = struct.unpack(formatString,readBuf)
    ycoor = np.multiply(ycoor,charLength)


    formatString = precision_Scalar[0]+str(nNodeZ)+precision_Scalar[1:]
    readBuf = f.read(struct.calcsize(formatString))
    zcoor = struct.unpack(formatString,readBuf)
    zcoor = np.multiply(zcoor,charLength)
    
    #processor coordinate bounds
    xc = xcoor[ix]
    yc = ycoor[iy]
    zc = zcoor[iz]

    return nProcX,nProcY,nProcZ,nNodeX,nNodeY,nNodeZ,ix,iy,iz,xcoor,ycoor,zcoor,xc,yc,zc


from scipy import special
import math
import stlparser
import is_inside_mesh as im
import time as t
# add eps for functions
#nomenklatur poly= stl
# replace PhaseID with dictonary
def stl2Phase(lamemGrid, mesh, names, unitMesh = "km", fast = False,  eps = .001):
    
    runStart = t.time()
    label =np.arange(1,len(names)+1)
    phaseName = dict(zip(names,label))
    
   
    
    print("-----------------------")
    print("Transferring meshes to marker phases, this process may take a while...")
    print("-----------------------")
    
    
    #--- getting the LaMEM grid ---
    try:
        gridVals = np.array([lamemGrid.Xpart.flatten('C'),
                             lamemGrid.Ypart.flatten('C'), lamemGrid.Zpart.flatten('C') ])
    except:
        print("please pass the whole lamemGrid class to this function")
        sys.exit(0)
    totalCoors = len(lamemGrid.Xpart.flatten('C'))
    
    # ---initalizing Phase and grid---
    Phase = np.zeros((totalCoors))          
    grid = np.zeros((totalCoors,3))
    
    # --- transposing 3D-arrays into list of coordinates ---
    
    if unitMesh == "km":
        grid = np.transpose(gridVals)
    elif unitMesh == "m":
        grid = np.transpose(gridVals)*1e3
    else:
        print("Coordinate unit in .stl file must be defined as m or km")
        sys.exit(0)
            
    # --- check for all stl files provided ---
    if isinstance(mesh,list):
        
        # --- point cloud plotting not supported for lists of meshes ---
        points = np.array([])
        
        for k in range(len(mesh)):
            # --- initializing the mesh ---
             # --- check for correct input format(.stl) ---
            #ipdb.set_trace()
            if ".stl" not in mesh[k]:
                    print("Error: mesh must be given as .stl file")
                    sys.exit(0)
            meshGrid = open(str(mesh[k]), "r")
            
            print("Transferring " + str(names[k]) + " as Phase number " +str(k+1))
            print("-----------------------")
            triangles =  np.array([X for X, N in stlparser.load(meshGrid)])
                # --- find markers in mesh ---
            if fast == False:
                for p in range(grid.shape[0]):
                    if im.is_inside_naive(triangles, grid[p,:], eps):
                          Phase[p] = k+1                      
                          points = np.append([points], grid[p,:])
            if fast == True:
                bools = im.is_inside_turbo(triangles, grid,eps)
                for p in range(grid.shape[0]):
                    if bools[p] == True:
                          Phase[p] = k+1
                          points = np.append([points], grid[p,:])
                         
    
    # --- if only one stl file is used ---
    else:
         # --- check for correct input format(.stl) ---
        if ".stl" not in mesh:
                print("Error: mesh must be given as .stl file")
                sys.exit(0)
        PhaseID = 1
        # --- initializing the mesh and a point cloud used for plotting ---
        points = np.array([])
        mesh = open(str(mesh), "r")
        # --- reading 3d mesh ---
        triangles =  np.array([X for X, N in stlparser.load(mesh)])
        
        # --- find markers in mesh ---
        if fast == False:
            #grid = np.array([p for p in grid if is_inside(triangles, p)]) 
            for p in range(grid.shape[0]):
                if im.is_inside_naive(triangles, grid[p,:], eps):
                      Phase[p] = PhaseID
                      points = np.append([points], grid[p,:])
        
        if fast == True:
    
            bools = im.is_inside_turbo(triangles, grid, eps)
            #sys.exit(0)
            for p in range(grid.shape[0]):
                if bools[p] == True:
                      Phase[p] = PhaseID
                      points = np.append([points], grid[p,:])
                  
    # --- assign phase to markers ---                  
    # --- BUG --- array reshape wrong!!
    size = np.shape(lamemGrid.Phase)
    size = np.array(size)
    #debugging, remove hashtag later
    # --- Transposing just gives flattend figures, holes still existent
    #Phase =np.transpose(Phase)
    Phase =np.reshape(Phase,(size),order='C')
    
    runEnd = t.time()
    runTime = runEnd - runStart
    print(str(len(points)/3)+ " matching cells found")
    print("------------------------")
    print("Finished, time elapsed to transfer meshs:")
    print(str(runTime) + " sec")
    print("------------------------")
    
    return Phase , points, phaseName
        

def halfSpaceCooling(lamemGrid, ageMyrs, tMantle, tSurf = 0):

    z = lamemGrid.Zpart.flatten('C') * 1e3
    kappa = 1e-6
    secYear = 3600*24*365.25
    Temp = np.zeros_like(z)
    for i in range(len(temp)):
        Temp[i] = tSurf + (tMantle-tsurf)*special.erf((z[i]/math.sqrt((kappa*ageMyrs*secYear*1e6))))
    
    Temp = np.reshape(Temp(len(temp)/3))
        
    
    
    return Temp







import subprocess



def createLaMEMinput(coord_x = "0.0 2.0", coord_y = "0.0 2.0",
                     coord_z = "0.0 2.0", n_phase = 6, rho = 2000, eta = 1e-19, 
                     inputName="input.dat",  out = "Block",  **kwargs):
    
    """
    Parameters
    ----------
    inputName : str
        Name and path where the inputfile should be saved. Subdirs must already exist
    coord_x, y, z:
        Start and end coors of the LaMEM grid. Must be smaller than GemPy grid, unit =km
    n_phase: int
        number of different phases
    out: str
        name and dir of the ParaView-output files, subdirs must already exist(maybe change later)
        
    **kwargs:
        Any Parameter listed in the default input file(LaMEM_default/input.dat) can be modified here

    Returns LaMEM inoutfile
    -------    
    """
    file = open("LaMEM_default/input.dat")
    content = file.readlines()
    index = np.array([])
    #ipdb.set_trace()
    for i in range(0, len(content)):
        if "coord_x" in content[i] and content[i][0] !="#":
            content[i] = "\t" + "coord_x" + " = " + coord_x
            index = np.append([index], i)
        elif "coord_y" in content[i] and content[i][0] !="#":
            content[i] = "\t" +"coord_y" + " = " + coord_y
            index = np.append([index], i)
        elif "coord_z" in content[i] and content[i][0] !="#":
            content[i] = "\t" +"coord_z" + " = " + coord_z
            index = np.append([index], i)
        elif "out_file_name" in content[i] and content[i][0]:
            content[i] = "\t" + "out_file_name" " = " +str(out)
            index = np.append([index], i)
    
    
    

    keys = list(kwargs.keys())
    values = np.atleast_1d(list(kwargs.values()))
    for i in range(0, len(content)):
        for j in range(0, len(keys)):
            if keys[j] in content[i] and content[i][0]!= "#":
                content[i] = "\t" + str(keys[j])+" = "+str(values[j])
                index = np.append([index], i)
                
                
    if n_phase != 0:
        
        for i in range(0,len(content)):
            if "# Define properties of block" in content[i]:
                for j in range(0, int(n_phase)):
                    content.insert(i+1,"#placeholder")
                    content.insert(i+1, "\n" + "\t"+"<MaterialEnd>")
                    content.insert(i+1, "\n" +"\t" + "\t" + "eta = " + str(eta) + " # viscosity")
                    content.insert(i+1, "\n" +"\t" + "\t" + "rho = " + str(rho) +"  # density")
                    content.insert(i+1, "\n" + "\t" + "\t" + "ID " + "= " +str(int(n_phase)-j) + "# Phase id")
                    content.insert(i+1, "\n" + "\t" + "\t" + "ch " + "= " +"2e7" + "# cohesion")
                    content.insert(i+1, "\n" + "\t" + "\t" + "fr " + "= " +"20.0" + "# friction angle")
                    content.insert(i+1, "\n" + "\t" + "\t" + "chSoftID " + "= " +"0" + "# friction softening law ID")
                    content.insert(i+1, "\n" + "\t" + "\t" + "frSoftID " + "= " +"0" + "# cohesion softening law ID")
                    content.insert(i+1,"\n" +"\t"+"<MaterialStart>")
                    
                    
                    
                    
                
    inputFile = open(inputName, "w")
    for i in range(0, len(content)):
        inputFile.write(content[i])
        if i in index[:]:
            inputFile.write("\n")
   

    inputFile.close()
    return inputName



def createPartitioning(n_proc, inputName,
                       Lamem = "/home/lucas/Libraries/LaMEM/bin/opt/LaMEM",):
    """
    Parameters
    ----------
    n_proc : int
        Number of processors
    inputName : string
        Path and name of the LaMEM inputfile
    Lamem : string
        Path to LaMEM installation The default is "/home/lucas/Software/LaMEM/bin/opt/LaMEM".

    Returns
    -------
    fname : filename
        Name of the ProcessorPartitioning File

    """
    #wd = os.getcwd()
    fol = inputName.count('/')
    directory = inputName.split('/')
    if fol == 1:
        
        infile = directory[1]
        folder = directory[0]
        os.chdir(folder)
    
        subprocess.run(["mpiexec", "-n", str(n_proc), Lamem, "-ParamFile", str(infile),
                        "-mode", "save_grid"])
        files = os.listdir()
        
        # #fuuuuck
    elif fol > 1:
        #get the last elemnt??
        infile = directory[-1]
        del directory[-1]
        folder = directory
        #ipdb.set_trace()
        folder = '/'.join(folder)
        os.chdir(folder)     
        
        subprocess.run(["mpiexec", "-n", str(n_proc), Lamem, "-ParamFile", str(infile),
                    "-mode", "save_grid"])
        files = os.listdir()
    else:
        print("checkpoint")                            
        subprocess.run(["mpiexec", "-n", str(n_proc), str(Lamem), "-ParamFile", str(inputName),
                        "-mode", "save_grid"])
        files = os.listdir()
        print(files) 
    for i in range(0,len(files)):
        PP = files[i].count('ProcessorPartitioning')
        if PP ==1:
            index = i
           
    fname = files[index]
    return fname

def readParams(paramfile):

    f = open(paramfile)
    
    params = f.readlines()
    index = np.array([])
    
    for i in range(len(params)):
        comm = params[i].count('#')
        if comm >= 1:
            params[i] =params[i].split('#')
            if len(params[i])==2: 
                del params[i][1]
    
    
    for i in range(len(params)):
        if type(params[i])==list:
            params[i] ="".join(params[i])
            
    
    for i in range(0, len(params)):
        units = params[i].count('units')    
        coor = params[i].count('coord_')
        partx = params[i].count('nmark_x')
        party = params[i].count('nmark_y')
        partz= params[i].count('nmark_z')
        nelx = params[i].count('nel_x')
        nely = params[i].count('nel_y')
        nelz = params[i].count('nel_z')
        if coor == 1:
            index = np.append([index], i)
        elif units == 1 :
            index = np.append([index], i)
        elif partx ==1:
            index = np.append([index], i)
        elif party ==1:
            index = np.append([index], i)
        elif partz ==1:
            index = np.append([index], i)
        elif nelx ==1 :
            index = np.append([index], i)
        elif nely ==1 :
            index = np.append([index], i)
        elif nelz ==1 :
            index = np.append([index], i)
        
    param = list(params[i] for i in index.astype(int))
    
    
    for i in range(len(param)):
        param[i] = param[i].split('=')
    
    parameter = []    
    values = []
    
    for i in range(len(param)):
        dummy = param[i]
        
        parameter.append(dummy[0])
        values.append(dummy[1])
    
    for i in range(len(parameter)):
        xcoor1 = parameter[i].count('coord_x')
        ycoor1 = parameter[i].count('coord_y')
        zcoor1 = parameter[i].count('coord_z')
        units1 = parameter[i].count('units')
        part_x1 = parameter[i].count('nmark_x')
        part_y1 = parameter[i].count('nmark_y')
        part_z1 = parameter[i].count('nmark_z')
        nelx = parameter[i].count('nel_x')
        nely = parameter[i].count('nel_y')
        nelz = parameter[i].count('nel_z')
        
        if xcoor1 ==1 :
            xcoor = values[i]
        elif ycoor1 ==1 :
            ycoor = values[i]
        elif ycoor1 ==1 :
            ycoor = values[i]
        elif zcoor1 ==1 :
            zcoor = values[i]
        elif units1 == 1 :
            units = values[i]
        elif part_x1 == 1:
            npart_x = values[i]
        elif part_y1 == 1:
            npart_y = values[i]
        elif part_z1 == 1:
            npart_z = values[i]
        elif nelx == 1:
            nel_x = values[i]
        elif nely == 1:
            nel_y = values[i]
        elif nelz == 1:
            nel_z = values[i]
            
    nel_x = float(nel_x)
    nel_x = int(nel_x)
    nel_y = float(nel_y)
    nel_y = int(nel_y)       
    nel_z = float(nel_z)
    nel_z = int(nel_z)       
                   
            
    
    
    nmark_x = int(npart_x)
    
    nmark_y = int(npart_y)
    
    nmark_z = int(npart_z)
 
    xcoor = xcoor.split()
    xcoor_start = float(xcoor[0])
    xcoor_start = round(xcoor_start, 2)
    xcoor_end = float(xcoor[1])
    xcoor_end = round(xcoor_end, 2)
    
    ycoor = ycoor.split()
    ycoor_start = float(ycoor[0])
    ycoor_start = round(ycoor_start, 2)
    ycoor_end = float(ycoor[1])
    ycoor_end = round(ycoor_end, 2)
    
    zcoor = zcoor.split()
    zcoor_start = float(zcoor[0])
    zcoor_start = round(zcoor_start, 2)
    zcoor_end = float(zcoor[1])
    zcoor_end = round(zcoor_end, 2)
    
    units = str(units)
    
    if 'none' in units:
        print("Error: Dimension of units is not defined")
        sys.exit()
    
    
    
    LaMEM_start = np.array([xcoor_start, ycoor_start, zcoor_start])
    LaMEM_end = np.array([xcoor_end, ycoor_end, zcoor_end])
    
    return units, LaMEM_start, LaMEM_end, nmark_x, nmark_y, nmark_z, nel_x, nel_y, nel_z 





def createLaMEMinput(coord_x = "0.0 2.0", coord_y = "0.0 2.0",
                     coord_z = "0.0 2.0", n_phase = 6, rho = 2000, eta = 1e-19, 
                     inputName="input.dat",  out = "Block",  **kwargs):
    
    """
    Parameters
    ----------
    inputName : str
        Name and path where the inputfile should be saved. Subdirs must already exist
    coord_x, y, z:
        Start and end coors of the LaMEM grid. Must be smaller than GemPy grid, unit =km
    n_phase: int
        number of different phases
    out: str
        name and dir of the ParaView-output files, subdirs must already exist(maybe change later)
        
    **kwargs:
        Any Parameter listed in the default input file(LaMEM_default/input.dat) can be modified here

    Returns LaMEM inoutfile
    -------    
    """
    file = open("LaMEM_default/input.dat")
    content = file.readlines()
    index = np.array([])
    #ipdb.set_trace()
    for i in range(0, len(content)):
        if "coord_x" in content[i] and content[i][0] !="#":
            content[i] = "\t" + "coord_x" + " = " + coord_x
            index = np.append([index], i)
        elif "coord_y" in content[i] and content[i][0] !="#":
            content[i] = "\t" +"coord_y" + " = " + coord_y
            index = np.append([index], i)
        elif "coord_z" in content[i] and content[i][0] !="#":
            content[i] = "\t" +"coord_z" + " = " + coord_z
            index = np.append([index], i)
        elif "out_file_name" in content[i] and content[i][0]:
            content[i] = "\t" + "out_file_name" " = " +str(out)
            index = np.append([index], i)
    
    
    

    keys = list(kwargs.keys())
    values = np.atleast_1d(list(kwargs.values()))
    for i in range(0, len(content)):
        for j in range(0, len(keys)):
            if keys[j] in content[i] and content[i][0]!= "#":
                content[i] = "\t" + str(keys[j])+" = "+str(values[j])
                index = np.append([index], i)
                
                
    if n_phase != 0:
        
        for i in range(0,len(content)):
            if "# Define properties of block" in content[i]:
                for j in range(0, int(n_phase)):
                    content.insert(i+1,"#placeholder")
                    content.insert(i+1, "\n" + "\t"+"<MaterialEnd>")
                    content.insert(i+1, "\n" +"\t" + "\t" + "eta = " + str(eta) + " # viscosity")
                    content.insert(i+1, "\n" +"\t" + "\t" + "rho = " + str(rho) +"  # density")
                    content.insert(i+1, "\n" + "\t" + "\t" + "ID " + "= " +str(int(n_phase)-j) + "# Phase id")
                    content.insert(i+1, "\n" + "\t" + "\t" + "ch " + "= " +"2e7" + "# cohesion")
                    content.insert(i+1, "\n" + "\t" + "\t" + "fr " + "= " +"20.0" + "# friction angle")
                    content.insert(i+1, "\n" + "\t" + "\t" + "chSoftID " + "= " +"0" + "# friction softening law ID")
                    content.insert(i+1, "\n" + "\t" + "\t" + "frSoftID " + "= " +"0" + "# cohesion softening law ID")
                    content.insert(i+1,"\n" +"\t"+"<MaterialStart>")
                    
                    
                    
                    
                
    inputFile = open(inputName, "w")
    for i in range(0, len(content)):
        inputFile.write(content[i])
        if i in index[:]:
            inputFile.write("\n")
   

    inputFile.close()
    return inputName



def createPartitioning(n_proc, inputName,
                       Lamem = "/home/lucas/Libraries/LaMEM/bin/opt/LaMEM",):
    """
    Parameters
    ----------
    n_proc : int
        Number of processors
    inputName : string
        Path and name of the LaMEM inputfile
    Lamem : string
        Path to LaMEM installation The default is "/home/lucas/Software/LaMEM/bin/opt/LaMEM".

    Returns
    -------
    fname : filename
        Name of the ProcessorPartitioning File

    """
    #wd = os.getcwd()
    fol = inputName.count('/')
    directory = inputName.split('/')
    if fol == 1:
        
        infile = directory[1]
        folder = directory[0]
        os.chdir(folder)
    
        subprocess.run(["mpiexec", "-n", str(n_proc), Lamem, "-ParamFile", str(infile),
                        "-mode", "save_grid"])
        files = os.listdir()
        
        # #fuuuuck
    elif fol > 1:
        #get the last elemnt??
        infile = directory[-1]
        del directory[-1]
        folder = directory
        #ipdb.set_trace()
        folder = '/'.join(folder)
        os.chdir(folder)     
        
        subprocess.run(["mpiexec", "-n", str(n_proc), Lamem, "-ParamFile", str(infile),
                    "-mode", "save_grid"])
        files = os.listdir()
    else:
        print("checkpoint")                            
        subprocess.run(["mpiexec", "-n", str(n_proc), str(Lamem), "-ParamFile", str(inputName),
                        "-mode", "save_grid"])
        files = os.listdir()
        print(files) 
    for i in range(0,len(files)):
        PP = files[i].count('ProcessorPartitioning')
        if PP ==1:
            index = i
           
    fname = files[index]
    return fname




      
      

        
        
           
    




