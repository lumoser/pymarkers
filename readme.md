This is a simple program to manipulate LaMEM input geometries.

Dependencies:
             - NumPy
             - SciPy
             - UVW - Universal VTK Writer (https://pypi.org/project/uvw/)
             
             
Main Features :
            - Import Geometries from 3D meshes from .stl files to LaMEM grid
            - Interpolating Phase on the LaMEM grid
            - Writing a VTK file of phases
            - Saving markerfiles for LaMEM parallel